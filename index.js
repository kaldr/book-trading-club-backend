const process = require('process');

const {app} = require('./src/app');

const server = app.listen(8080, '127.0.0.1', () => console.log('Listening at http://localhost:8080/'));

process.on('SIGINT', () => {
  console.log(`Got SIGINT`, new Date().toISOString());
  shutdown();
});

process.on('SIGTERM', () => {
  console.log(`Got SIGTERM`, new Date().toISOString());
  shutdown();
});

function shutdown() {
  server.close(err => {
    if (err) {
      console.error(err);
      process.exitCode = 1;
    }
    process.exit();
  });
}