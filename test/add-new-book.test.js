const test = require('tape');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const {addNewBook} = require('../src/add-new-book');

test('book is not valid', async t => {
  t.plan(1);

  const book = {
    id: undefined,
    title: 'title',
    thumbnailUrl: 'url',
  };

  const request = httpMocks.createRequest({
    body: book,
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await addNewBook()(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('book added successfully', async t => {
  t.plan(1);

  const addToDB = sinon.stub().resolves();

  const book = {
    id: '0',
    title: 'title',
    thumbnailUrl: 'url',
  };

  const request = httpMocks.createRequest({
    body: book,
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await addNewBook(addToDB)(request, response);

  t.equal(response.statusCode, 201);

  t.end();
});