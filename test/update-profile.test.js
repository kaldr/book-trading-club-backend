const test = require('tape');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const {update} = require('../src/profile');

test('update profile', async t => {
  t.plan(1);

  const updateProfile = sinon.stub().resolves();

  const request = httpMocks.createRequest({
    user: {
      id: '0',
      name: 'name',
    },
    body: {
      fullname: 'name',
      city: 'city',
      state: undefined,
    },
  });
  const response = httpMocks.createResponse();

  await update(updateProfile)(request, response);

  t.equal(response.statusCode, 200);

  t.end();
});