const test = require('tape');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const {initState} = require('../src/init-state');

test('anonymous user init state', async t => {
  t.plan(3);

  const getAllBooks = sinon.stub().resolves(['book']);

  const request = httpMocks.createRequest();
  const response = httpMocks.createResponse();

  await initState(getAllBooks)(request, response);
  const data = response._getData();

  t.equal(response.statusCode, 200);
  t.equal(data.name, '');
  t.deepEqual(data.books, ['book']);

  t.end();
});

test('authenticated user init state', async t => {
  t.plan(3);

  const getAllBooks = sinon.stub().resolves(['book']);

  const request = httpMocks.createRequest({
    user: {
      id: 0,
      name: 'user',
    },
  });
  const response = httpMocks.createResponse();

  await initState(getAllBooks)(request, response);
  const data = response._getData();

  t.equal(response.statusCode, 200);
  t.equal(data.name, 'user');
  t.deepEqual(data.books, ['book']);

  t.end();
});