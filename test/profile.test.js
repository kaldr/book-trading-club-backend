const test = require('tape');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const {profile} = require('../src/profile');

test('profile info', async t => {
  t.plan(2);

  const profileInfo = sinon.stub().resolves({info: 'info'});

  const request = httpMocks.createRequest({
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await profile(profileInfo)(request, response);
  const data = response._getData();

  t.equal(response.statusCode, 200);
  t.deepEqual(data, {info: 'info'});

  t.end();
});