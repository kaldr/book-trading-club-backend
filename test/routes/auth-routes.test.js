const request = require('supertest');
const test = require('tape');

const {seed} = require('../../src/db/seeds/development/testing-data');
const {findUserByName} = require('../../src/db/queries');
const {query} = require('../../src/db/db');
const {app, urlPrefix} = require('../../src/app');

test('signup: you must not be an authenticated user', async t => {
  await seed();

  const name = 'test1';

  const signinRes = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: name, password: '1234'});

  const newUsername = 'newUsername';

  const res = await request(app)
    .post(`${urlPrefix}/signup`)
    .set('Authorization', signinRes.header.authorization)
    .send({username: newUsername, password: 'password'});

  t.equal(res.status, 400);

  const newUser = await findUserByName(newUsername);
  t.equal(newUser, undefined);

  t.end();
});

test('signup: username and password fields can not be empty', async t => {
  await seed();

  const newUsername = 'newUsername';

  let res = await request(app)
    .post(`${urlPrefix}/signup`)
    .send({username: newUsername, password: ''});

  t.equal(res.status, 400);

  let newUser = await findUserByName(newUsername);
  t.equal(newUser, undefined);

  res = await request(app)
    .post(`${urlPrefix}/signup`)
    .send({username: '', password: 'password'});

  t.equal(res.status, 400);

  newUser = await findUserByName(newUsername);
  t.equal(newUser, undefined);

  t.end();
});

test('signup: username must be unique', async t => {
  await seed();

  const newUsername = 'test1'; // already taken

  const res = await request(app)
    .post(`${urlPrefix}/signup`)
    .send({username: newUsername, password: 'password'});

  t.equal(res.status, 401);
  t.equal(res.body.message, 'username already taken');
  t.equal(res.header['authorization'], undefined);

  t.end();
});

test('signup: successful', async t => {
  await seed();

  const newUsername = 'newUsername';

  const res = await request(app)
    .post(`${urlPrefix}/signup`)
    .send({username: newUsername, password: 'password'});

  t.equal(res.status, 200);
  t.true(!!res.header['authorization']);
  
  const newUser = await findUserByName(newUsername.trim().toLowerCase());
  t.equal(newUser.name, newUsername.trim().toLowerCase());

  t.end();
});

test('signin: you must not be an authenticated user', async t => {
  await seed();

  const name = 'test1';

  const signinRes = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: name, password: '1234'});

  const res = await request(app)
    .post(`${urlPrefix}/signin`)
    .set('Authorization', signinRes.header.authorization)
    .send({username: 'username', password: 'password'});

  t.equal(res.status, 400);
  t.false(!!res.header['authorization']);

  t.end();
});

test('signin: username and password fields can not be empty', async t => {
  await seed();

  let res = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: 'username', password: ''});

  t.equal(res.status, 400);
  t.false(!!res.header['authorization']);

  res = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: '', password: 'password'});

  t.equal(res.status, 400);
  t.false(!!res.header['authorization']);

  t.end();
});

test('signin: username is wrong', async t => {
  await seed();

  const username = 'test3'; // this user in mot there

  const res = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username, password: 'password'});

  t.equal(res.status, 401);
  t.equal(res.body.message, 'username is incorrect');
  t.false(!!res.header['authorization']);

  t.end();
});

test('signin: password is wrong', async t => {
  await seed();

  const username = 'test1';

  const res = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username, password: 'wrong password'});

  t.equal(res.status, 401);
  t.equal(res.body.message, 'password is incorrect');
  t.false(!!res.header['authorization']);

  t.end();
});

test('signin: successful', async t => {
  await seed();

  const username = 'test1';

  const res = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: username, password: '1234'});

  t.equal(res.status, 200);
  t.true(!!res.header['authorization']);

  t.end();
});

test('signout: user must be authenticated', async t => {
  await seed();

  const res = await request(app)
    .post(`${urlPrefix}/signout`);

  t.equal(res.status, 401);
  t.equal(res.header['authorization'], undefined);

  t.end();
});

test('signout: successful', async t => {
  await seed();

  const name = 'test1';

  const signinRes = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: name, password: '1234'});

  const res = await request(app)
    .post(`${urlPrefix}/signout`)
    .set('Authorization', signinRes.header.authorization);

  t.equal(res.status, 200);
  t.equal(res.header['authorization'], '');

  const {rows: [{tokenHash}]} = await query(
    `SELECT user_refresh_token_random_token_hash AS "tokenHash"
    FROM book_trading_club.user WHERE user_name = $1`,
    [name]
  );
  t.equal(tokenHash, null);

  t.end();
});