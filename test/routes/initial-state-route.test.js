const request = require('supertest');
const test = require('tape');

const {seed} = require('../../src/db/seeds/development/testing-data');
const {app, urlPrefix} = require('../../src/app');

test('route: anonymous user init state', async t => {
  await seed();

  const name = '';

  const res = await request(app)
    .get(`${urlPrefix}/initstate`);

  t.equal(res.body.name, name);
  t.equal(res.body.books.length, 3);

  t.end();
});

test('route: authenticated user init state', async t => {
  await seed();

  const name = 'test1';

  const signinRes = await request(app)
    .post(`${urlPrefix}/signin`)
    .send({username: name, password: '1234'});

  const res = await request(app)
    .get(`${urlPrefix}/initstate`)
    .set('Authorization', signinRes.header.authorization);

  t.equal(res.body.name, name);
  t.equal(res.body.books.length, 3);

  t.end();
});