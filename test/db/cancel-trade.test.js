const test = require('tape');

const {cancelTrade} = require('../../src/db/queries');
const db = require('../../src/db/db');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('cancel trade', async t => {
  t.plan(1);
  await seed();

  const tradeId = 'b31e5f17-358c-45a2-b9ae-03e4c542f692';
  await cancelTrade(tradeId);

  const {rows: [{count}]} = await db.query(
    `SELECT count(*) FROM book_trading_club.trade WHERE trade_id = $1;`,
    [tradeId]
  );

  t.equal(count, '0');

  t.end();
});