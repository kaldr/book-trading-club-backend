const test = require('tape');

const {addTrade} = require('../../src/db/queries');
const db = require('../../src/db/db');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('add new trade', async t => {
  t.plan(1);
  await seed();

  const userId = '251e3325-920a-4147-a531-a68d0831f210'; // test2
  const userBookId = 'd702c47f-a336-447e-b16a-1df47dac4383';

  const tradeId = await addTrade(userId, userBookId);

  const {rows: [trade]} = await db.query(
    `SELECT trade_id AS "tradeId", trade_initiater_id AS "userId", user_book_id AS "userBookId"
     FROM book_trading_club.trade WHERE trade_id = $1;`,
    [tradeId]
  );

  t.deepEqual(trade, {
    tradeId,
    userId,
    userBookId,
  });

  t.end();
});