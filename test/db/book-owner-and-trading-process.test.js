const test = require('tape');

const {bookOwnerAndIsBookInTradingProcess} = require('../../src/db/queries');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('test book owner and is book in trading process', async t => {
  t.plan(3);
  await seed();

  let userBookId = 'd702c47f-a336-447e-b16a-1df47dac4383';
  let result = await bookOwnerAndIsBookInTradingProcess(userBookId);
  t.deepEqual(result, {
    owner: 'b5ec86cc-b78c-4f37-8335-07633fac8d5d',
    bookIsInTradingProcess: false,
  });

  userBookId = '9029e7f6-782c-405c-b5f8-cfe58fac9236';
  result = await bookOwnerAndIsBookInTradingProcess(userBookId);
  t.deepEqual(result, {
    owner: '251e3325-920a-4147-a531-a68d0831f210',
    bookIsInTradingProcess: true,
  });

  userBookId = '7029e7f6-782c-405c-b5f8-cfe58fac7236'; // not existing id
  result = await bookOwnerAndIsBookInTradingProcess(userBookId);
  t.deepEqual(result, {
    owner: undefined,
    bookIsInTradingProcess: false,
  });

  t.end();
});