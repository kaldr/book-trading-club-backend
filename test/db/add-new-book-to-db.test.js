const test = require('tape');

const {addBookToDB} = require('../../src/db/queries');
const db = require('../../src/db/db');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('add new book', async t => {
  t.plan(2);
  await seed();

  const userId = '251e3325-920a-4147-a531-a68d0831f210'; // test2
  const book = {
    id: 'id',
    title: 'title',
    thumbnailUrl: 'url',
  };
  const userBookId = await addBookToDB(userId, book);

  const {rows: [userBookFromDB]} = await db.query(
    `SELECT user_book_id as "userBookId", book_id AS "bookId", user_id AS "userId"
     FROM book_trading_club.user_book WHERE user_book_id = $1;`,
    [userBookId]
  );

  const {rows: [bookFromDB]} = await db.query(
    `SELECT book_id AS "id", book_title AS "title", book_thumbnail_url AS "thumbnailUrl"
     FROM book_trading_club.book WHERE book_id = $1;`,
    [book.id]
  );

  t.deepEqual(userBookFromDB, {
    userBookId,
    bookId: book.id,
    userId,
  });

  t.deepEqual(bookFromDB, book);

  t.end();
});

test('book thumbnail url can be empty', async t => {
  t.plan(1);
  await seed();

  const userId = '251e3325-920a-4147-a531-a68d0831f210'; // test2
  const book = {
      id: 'id',
      title: 'title',
      // thumbnailUrl: undefined,
  };
  await addBookToDB(userId, book);

  const {rows: [bookFromDB]} = await db.query(
    `SELECT book_id AS "id", book_title AS "title", book_thumbnail_url AS "thumbnailUrl"
     FROM book_trading_club.book WHERE book_id = $1;`,
    [book.id]
  );

  t.deepEqual(bookFromDB, {
    id: book.id,
    title: book.title,
    thumbnailUrl: null,
  });

  t.end();
});