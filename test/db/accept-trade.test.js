const test = require('tape');

const {acceptTrade} = require('../../src/db/queries');
const db = require('../../src/db/db');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('accept trade', async t => {
  t.plan(1);
  await seed();

  const tradeId = 'b31e5f17-358c-45a2-b9ae-03e4c542f692';
  await acceptTrade(tradeId);

  const {rows: [{accepted}]} = await db.query(
    `SELECT trade_accepted AS "accepted" FROM book_trading_club.trade WHERE trade_id = $1;`,
    [tradeId]
  );
  t.equal(accepted, true);

  t.end();
});