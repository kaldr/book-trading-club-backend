const test = require('tape');

const {updateProfile} = require('../../src/db/queries');
const db = require('../../src/db/db');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('add info to user without info', async t => {
  t.plan(1);
  await seed();

  const userId = '251e3325-920a-4147-a531-a68d0831f210'; // test2
  const userInfo = {
    fullname: 'fullname',
    city: 'city',
    // state: 'state',
  };
  await updateProfile(userId, userInfo);

  const {rows: [userData]} = await db.query(
    `SELECT user_full_name AS "fullname", user_city AS "city", user_state AS "state"
     FROM book_trading_club.user WHERE user_id = $1;`,
    [userId]
  );

  t.deepEqual(userData, {
    fullname: userInfo.fullname,
    city: userInfo.city,
    state: null,
  });

  t.end();
});

test('update existing info', async t => {
  t.plan(1);
  await seed();

  const userId = 'b5ec86cc-b78c-4f37-8335-07633fac8d5d'; // test1
  const userInfo = {
    fullname: 'new fullname',
    city: 'new city',
    state: 'State',
  };
  await updateProfile(userId, userInfo);
  
  const {rows: [userData]} = await db.query(
    `SELECT user_full_name AS "fullname", user_city AS "city", user_state AS "state"
     FROM book_trading_club.user WHERE user_id = $1;`,
    [userId]
  );

  t.deepEqual(userData, {
    fullname: userInfo.fullname,
    city: userInfo.city,
    state: 'State',
  });

  t.end();
});