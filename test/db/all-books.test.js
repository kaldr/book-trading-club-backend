const test = require('tape');

const {getAllBooks} = require('../../src/db/queries');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('get all books', async t => {
  t.plan(4);
  await seed();

  const book = {
    owner: 'test1',
    userBookId: '9b7a98e3-7089-48c5-aae7-a0341cd73dbd',
    tradeAccepted: true,
    id: 'pD6arNyKyi8C',
    title: 'The Hobbit',
    thumbnailUrl: 'http://books.google.com/books/content?id=pD6arNyKyi8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
  };

  const books = await getAllBooks();

  t.equal(books.length, 3);
  t.deepEqual(books[0], book);
  t.equal(books[1].tradeAccepted, null);
  t.equal(books[2].tradeAccepted, false);

  t.end();
});