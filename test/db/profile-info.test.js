const test = require('tape');

const {profileInfo} = require('../../src/db/queries');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('get profile info', async t => {
  t.plan(1);
  await seed();

  const userId = 'b5ec86cc-b78c-4f37-8335-07633fac8d5d'; // test1
  const profile = {
    userInfo: {
      fullname: 'John Due',
      city: 'City',
      state: 'State',
    },
    userTrades: [{
      accepted: false,
      id: 'aWZzLPhY4o0C',
      title: 'The Fellowship of the Ring',
      thumbnailUrl: 'http://books.google.com/books/content?id=aWZzLPhY4o0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      owner: 'test2',
    }],
    tradesForUser: [{
      accepted: true,
      id: 'pD6arNyKyi8C',
      title: 'The Hobbit',
      thumbnailUrl: 'http://books.google.com/books/content?id=pD6arNyKyi8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api',
      tradeInitiater: 'test2',
      tradeId: '6fb30f3d-18b4-4101-a43a-1324a3dd45cc',
    }],
  };

  const result = await profileInfo(userId);
  t.deepEqual(result, profile);

  t.end();
});