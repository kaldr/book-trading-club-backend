const test = require('tape');

const {isTradeValid} = require('../../src/db/queries');
const {seed} = require('../../src/db/seeds/development/testing-data');

test('wrong trade id return false', async t => {
  t.plan(1);
  await seed();

  const userId = '251e3325-920a-4147-a531-a68d0831f210'; // test2
  const tradeId = '251e3325-920a-4147-a531-a68d0831f211'; // wrong trade id
  const result = await isTradeValid(tradeId, userId);

  t.equal(result, false);

  t.end();
});

test('user is not the owner of book', async t => {
  t.plan(1);
  await seed();

  const userId = 'b5ec86cc-b78c-4f37-8335-07633fac8d5d'; // test1
  const tradeId = 'b31e5f17-358c-45a2-b9ae-03e4c542f692';
  const result = await isTradeValid(tradeId, userId);

  t.equal(result, false);

  t.end();
});

test('trade must not be accepted yet', async t => {
  t.plan(1);
  await seed();

  const userId = 'b5ec86cc-b78c-4f37-8335-07633fac8d5d'; // test1
  const tradeId = '6fb30f3d-18b4-4101-a43a-1324a3dd45cc';
  const result = await isTradeValid(tradeId, userId);

  t.equal(result, false);

  t.end();
});

test('valid trade', async t => {
  t.plan(1);
  await seed();

  const userId = '251e3325-920a-4147-a531-a68d0831f210'; // test2
  const tradeId = 'b31e5f17-358c-45a2-b9ae-03e4c542f692';
  const result = await isTradeValid(tradeId, userId);

  t.equal(result, true);

  t.end();
});