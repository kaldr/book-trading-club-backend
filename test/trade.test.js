const test = require('tape');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const {trade} = require('../src/trade');

test('trade request with empty user book id', async t => {
  t.plan(1);

  const request = httpMocks.createRequest({
    body: {
      userBookId: undefined,
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await trade()(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('book is not there', async t => {
  t.plan(1);

  const invalidBook = sinon.stub().resolves({owner: undefined});
  const addTrade = sinon.stub().resolves();

  const request = httpMocks.createRequest({
    body: {
      userBookId: 'id is not there',
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await trade(invalidBook, addTrade)(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('trade requester is the owner of the book', async t => {
  t.plan(1);

  const ownBook = sinon.stub().resolves({owner: '0'});
  const addTrade = sinon.stub().resolves();

  const request = httpMocks.createRequest({
    body: {
      userBookId: 'this book is for user with 0 id',
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await trade(ownBook, addTrade)(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('requested book is already in trading process', async t => {
  t.plan(1);

  const alreadyInTradingProcess = sinon.stub().resolves({owner: 'non 0', bookIsInTradingProcess: true});
  const addTrade = sinon.stub().resolves();

  const request = httpMocks.createRequest({
    body: {
      userBookId: 'this book is already in trading process',
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await trade(alreadyInTradingProcess, addTrade)(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('trade added successfuly', async t => {
  t.plan(1);

  const validTrade = sinon.stub().resolves({owner: 'non 0', bookIsInTradingProcess: false});
  const addTrade = sinon.stub().resolves();

  const request = httpMocks.createRequest({
    body: {
      userBookId: 'valid book',
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await trade(validTrade, addTrade)(request, response);

  t.equal(response.statusCode, 201);

  t.end();
});