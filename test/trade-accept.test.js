const test = require('tape');
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const {accept} = require('../src/trade');

test('accept request with empty trade id', async t => {
  t.plan(1);

  const request = httpMocks.createRequest({
    params: {
      tradeId: undefined,
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await accept()(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('accept request with invalid trade(user in not owner, there is not trade, ...)', async t => {
  t.plan(1);

  const invalidTrade = sinon.stub().resolves(false);

  const request = httpMocks.createRequest({
    params: {
      tradeId: 'id',
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await accept(invalidTrade)(request, response);

  t.equal(response.statusCode, 400);

  t.end();
});

test('accept request with valid trade', async t => {
  t.plan(1);

  const validTrade = sinon.stub().resolves(true);
  const acceptTrade = sinon.stub().resolves();

  const request = httpMocks.createRequest({
    params: {
      tradeId: 'id',
    },
    user: {
      id: '0',
      name: 'name',
    },
  });
  const response = httpMocks.createResponse();

  await accept(validTrade, acceptTrade)(request, response);

  t.equal(response.statusCode, 200);

  t.end();
});