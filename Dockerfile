FROM node:12-slim as base
ENV NODE_ENV production
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENV PORT 8080
EXPOSE ${PORT}
RUN mkdir /app && chown -R node:node /app
WORKDIR /app
USER node
COPY --chown=node:node package.json package-lock*.json .
RUN npm ci && npm cache clean --force

FROM base as dev
ENV NODE_ENV development
ENV PATH=/app/node_modules/.bin:$PATH
RUN npm install --only=development
# CMD ["nodemon", "index.js"]
CMD ["node", "index.js"]

FROM base as source
COPY --chown=node:node . .

FROM source as test
ENV NODE_ENV development
ENV PATH=/app/node_modules/.bin:$PATH
COPY --from=dev /app/node_modules /app/node_modules
RUN eslint .
RUN npm test
CMD ["npm", "test"]

FROM source as prod
ENTRYPOINT ["/tini", "--"]
CMD ["node", "index.js"]

# docker build -t myapp .
# docker build -t myapp:prod --target prod