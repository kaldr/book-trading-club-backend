const express = require('express');

const {
  addBookToDB,
  getAllBooks,
  profileInfo,
  updateProfile,
  bookOwnerAndIsBookInTradingProcess,
  addTrade,
  isTradeValid,
  cancelTrade,
  acceptTrade,
  deleteRefreshToken,
  checkUserHasTheRefreshToken,
} = require('./db/queries');
const {jwtAuth, isAuthorized} = require('./auth/jwtAuth');
const {signUp, signIn, signOut} = require('./auth/authentication');
const {addNewBook} = require('./add-new-book');
const {initState} = require('./init-state');
const {profile, update} = require('./profile');
const {trade, cancel, accept} = require('./trade');

const router = express.Router();

// authenticate on all routes
const authenticate = jwtAuth(deleteRefreshToken, checkUserHasTheRefreshToken);
router.use(authenticate);

// auth routes
router.post('/signup', signUp);
router.post('/signin', signIn);
router.post('/signout', isAuthorized, signOut);

// books routes
router.post('/books', isAuthorized, addNewBook(addBookToDB));

// init state routes
router.get('/initstate', initState(getAllBooks));

// profile routes
router.get('/profile', isAuthorized, profile(profileInfo));
router.put('/profile', isAuthorized, update(updateProfile));

// trades routes
router.post('/trades', isAuthorized, trade(bookOwnerAndIsBookInTradingProcess, addTrade));
router.put('/trades/:tradeId/cancel', isAuthorized, cancel(isTradeValid, cancelTrade));
router.put('/trades/:tradeId/accept', isAuthorized, accept(isTradeValid, acceptTrade));

module.exports = router;