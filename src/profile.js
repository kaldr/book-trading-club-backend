const profile = profileInfo => async (req, res, next) => {
  try {
    const info = await profileInfo(req.user.id);
    return res.status(200).send(info);
  } catch (err) {
    return next(err);
  }
};

const update = updateProfile => async (req, res, next) => {
  try {
    const fullname = req.body.fullname ? req.body.fullname.trim() : undefined;
    const city = req.body.city ? req.body.city.trim() : undefined;
    const state = req.body.state ? req.body.state.trim() : undefined;

    await updateProfile(req.user.id, {fullname, city, state});
    return res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
};

module.exports = {
  profile,
  update,
};