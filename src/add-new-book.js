const addNewBook = addToDB => async (req, res, next) => {
  try {
    const id = req.body.id ? req.body.id.trim() : undefined;
    const title = req.body.title ? req.body.title.trim() : undefined;
    const thumbnailUrl = req.body.thumbnailUrl ? req.body.thumbnailUrl.trim() : undefined;

    if (!id || !title) {
      return res.sendStatus(400);
    }

    const book = {id, title, thumbnailUrl};

    await addToDB(req.user.id, book);
    return res.sendStatus(201);
  } catch (err) {
    return next(err);
  }
};

module.exports = {
  addNewBook,
};