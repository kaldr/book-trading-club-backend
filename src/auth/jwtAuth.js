const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const {promisify} = require('util');

const jwtSign = promisify(jwt.sign);
const jwtVerify = promisify(jwt.verify);
const randomBytes = promisify(crypto.randomBytes);

const bearerToken = (at, rt) => `Bearer at=${at}, rt=${rt}`;

const SECRET = 'secret';
const ACCESS_TOKEN_EXPIRES_IN = 60 * 15; // 15 Minutes
const REFRESH_TOKEN_EXPIRES_IN = 3600 * 24 * 30; // 1 Month

function parseHeader(header = '') {
  header = header.trim();
  const re = /^Bearer at=(\S+), ?rt=(\S+)$/;
  const match = re.exec(header);
  if (!match) {
    return undefined;
  }
  const [_, at, rt] = match;
  return {at, rt};
}

async function verifyAccessToken(accessToken) {
  try {
    const {user, exp} = await jwtVerify(accessToken, SECRET, {
      ignoreExpiration: true,
    });
    return {
      valid: true,
      user,
      expired: Date.now() >= exp * 1000,
    };
  } catch (err) {
    return {valid: false};
  }
}

async function verifyRefreshToken(refreshToken) {
  try {
    const {rt, exp} = await jwtVerify(refreshToken, SECRET, {
      ignoreExpiration: true,
    });
    return {
      valid: true,
      rt,
      expired: Date.now() >= exp * 1000,
    };
  } catch (err) {
    return {valid: false};
  }
}

const jwtAuth = (deleteRefreshToken, checkUserHasTheRefreshToken) => async (req, res, next) => {
  const authHeader = req.header('Authorization');
  const tokens = parseHeader(authHeader);
  if (!tokens) {
    req.user = undefined;
    return next();
  }
  const {at, rt} = tokens;

  const {valid, user, expired} = await verifyAccessToken(at);
  if (!valid) {
    req.user = undefined;
    res.header('Authorization', '');
    return next();
  }

  if (!expired) {
    req.user = user;
    return next();
  }

  const decodedRefreshToken = await verifyRefreshToken(rt);
  if (!decodedRefreshToken.valid) {
    req.user = undefined;
    res.header('Authorization', '');
    return next();
  }

  if (decodedRefreshToken.expired) {
    req.user = undefined;
    res.header('Authorization', '');
    try {
      await deleteRefreshToken(user.id);
      return next();
    } catch (err) {
      return next(err);
    }
  }

  try {
    const userHasTheRefreshToken = await checkUserHasTheRefreshToken(user.id, decodedRefreshToken.rt);
    if (!userHasTheRefreshToken) {
      req.user = undefined;
      res.header('Authorization', '');
      return next();
    }
    const newAccessToken = await jwtSign({user}, SECRET, {expiresIn: ACCESS_TOKEN_EXPIRES_IN});
    res.header('Authorization', bearerToken(newAccessToken, rt));
    req.user = user;
    return next();
  } catch (err) {
    req.user = undefined;
    res.header('Authorization', '');
    return next(err);
  }
}

async function newTokens(user) {
  const accessToken = await jwtSign({user}, SECRET, {expiresIn: ACCESS_TOKEN_EXPIRES_IN});
  const randomToken = (await randomBytes(48)).toString('hex');
  const refreshToken = await jwtSign({rt: randomToken}, SECRET, {expiresIn: REFRESH_TOKEN_EXPIRES_IN});
  return {at: accessToken, rt: refreshToken, randomToken};
}

function isAuthorized(req, res, next) {
  req.user ? next() : res.sendStatus(401);
}

module.exports = {
  jwtAuth,
  parseHeader,
  newTokens,
  bearerToken,
  isAuthorized,
};