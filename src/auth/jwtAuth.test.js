const test = require('tape');
const {createRequest, createResponse} = require('node-mocks-http');
const sinon = require('sinon');
const jwt = require('jsonwebtoken');
const {promisify} = require('util');

const {jwtAuth, parseHeader, isAuthorized} = require('./jwtAuth');

const jwtSign = promisify(jwt.sign);
const jwtVerify = promisify(jwt.verify);

test('there is not "Authorization" header req.user will be undefined', async t => {
  t.plan(1);
  async function next() {
    t.equal(req.user, undefined);
    t.end();
  }
  const req = createRequest();
  const res = createResponse();

  await jwtAuth()(req, res, next);
});

test('there is "Authorization" header but incorrect format, req.user will be undefined', async t => {
  t.plan(1);
  async function next() {
    t.equal(req.user, undefined);
    t.end();
  }
  const req = createRequest({
    headers: {
      'Authorization': 'incorrect format',
    },
  });
  const res = createResponse();

  await jwtAuth()(req, res, next);
});

test('invalid access token', async t => {
  t.plan(2);
  async function next() {
    t.equal(req.user, undefined);
    t.equal(res.getHeader('Authorization'), '');
    t.end();
  }
  const req = createRequest({
    headers: {
      'Authorization': 'Bearer at=invalid-access-token, rt=valid-or-not-valid-refresh-token',
    },
  });
  const res = createResponse();

  await jwtAuth()(req, res, next);
});

test('valid not expired access token', async t => {
  t.plan(1);
  const user = {id: 'id'};
  async function next() {
    t.deepEqual(req.user, user);
    t.end();
  }

  const accessToken = await jwtSign({user}, 'secret', {expiresIn: 10});
  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=valid-or-not-valid-refresh-token`,
    },
  });
  const res = createResponse();

  await jwtAuth()(req, res, next);
});

test('valid expired access token with invalid refresh token', async t => {
  t.plan(2);
  
  async function next() {
    t.deepEqual(req.user, undefined);
    t.equal(res.getHeader('Authorization'), '');
    t.end();
  }

  const accessToken = await jwtSign({ // expired token
    userId: 'id',
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });
  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=invalid-refresh-token`,
    },
  });
  const res = createResponse();

  await jwtAuth()(req, res, next);
});

// TODO: check async function awaited
test('valid expired access token with expired refresh token successful rt delete', async t => {
  t.plan(3);
  
  const user = {id: 'id'};
  const rt = 'rt';
  const deleteRefreshToken = sinon.stub().resolves();

  async function next() {
    t.deepEqual(req.user, undefined);
    t.equal(res.getHeader('Authorization'), '');
    t.ok(deleteRefreshToken.calledWith(user.id));
    t.end();
  }

  const accessToken = await jwtSign({ // expired token
    user,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const refreshToken = await jwtSign({ // expired token
    rt,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=${refreshToken}`,
    },
  });
  const res = createResponse();

  await jwtAuth(deleteRefreshToken)(req, res, next);
});

// TODO: check async function awaited
test('valid expired access token with expired refresh token failure rt delete', async t => {
  t.plan(4);
  
  const user = {id: 'id'};
  const rt = 'rt';
  const deleteRefreshToken = sinon.stub().rejects();

  async function next(err) {
    t.deepEqual(req.user, undefined);
    t.equal(res.getHeader('Authorization'), '');
    t.ok(deleteRefreshToken.calledWith(user.id));
    t.ok(err);
    t.end();
  }

  const accessToken = await jwtSign({ // expired token
    user,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const refreshToken = await jwtSign({ // expired token
    rt,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=${refreshToken}`,
    },
  });
  const res = createResponse();

  await jwtAuth(deleteRefreshToken)(req, res, next);
});

test('valid expired access token with valid refresh token, user does not have it', async t => {
  t.plan(3);
  
  const user = {id: 'id'};
  const rt = 'rt';
  const checkUserHasTheRefreshToken = sinon.stub().resolves(false);

  async function next() {
    t.deepEqual(req.user, undefined);
    t.equal(res.getHeader('Authorization'), '');
    t.ok(checkUserHasTheRefreshToken.calledWith(user.id, rt));
    t.end();
  }

  const accessToken = await jwtSign({ // expired token
    user,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const refreshToken = await jwtSign({ // valid token
    rt,
  }, 'secret', {
    expiresIn: 10,
  });

  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=${refreshToken}`,
    },
  });
  const res = createResponse();

  await jwtAuth(undefined, checkUserHasTheRefreshToken)(req, res, next);
});

test('valid expired access token with valid refresh token, user has it', async t => {
  t.plan(5);
  
  const user = {id: 'id'};
  const rt = 'rt';
  const checkUserHasTheRefreshToken = sinon.stub().resolves(true);

  async function next() {
    t.deepEqual(req.user, user);
    t.ok(checkUserHasTheRefreshToken.calledWith(user.id, rt));
    const tokens = parseHeader(res.getHeader('Authorization'));
    t.notEqual(tokens, undefined);
    t.equal(refreshToken, tokens.rt);
    const decoded = await jwtVerify(tokens.at, 'secret');
    t.equal(user.id, decoded.user.id);
    t.end();
  }

  const accessToken = await jwtSign({ // expired token
    user,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const refreshToken = await jwtSign({ // valid token
    rt,
  }, 'secret', {
    expiresIn: 10,
  });

  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=${refreshToken}`,
    },
  });
  const res = createResponse();

  await jwtAuth(undefined, checkUserHasTheRefreshToken)(req, res, next);
});

test('valid expired access token with valid refresh token, failure check', async t => {
  t.plan(4);
  
  const user = {id: 'id'};
  const rt = 'rt';
  const checkUserHasTheRefreshToken = sinon.stub().rejects();

  async function next(err) {
    t.deepEqual(req.user, undefined);
    t.equal(res.getHeader('Authorization'), '');
    t.ok(checkUserHasTheRefreshToken.calledWith(user.id, rt));
    t.ok(err);
    t.end();
  }

  const accessToken = await jwtSign({ // expired token
    user,
    iat: Math.floor(Date.now() / 1000) - 30,
  }, 'secret', {
    expiresIn: 10,
  });

  const refreshToken = await jwtSign({ // valid token
    rt,
  }, 'secret', {
    expiresIn: 10,
  });

  const req = createRequest({
    headers: {
      'Authorization': `Bearer at=${accessToken}, rt=${refreshToken}`,
    },
  });
  const res = createResponse();

  await jwtAuth(undefined, checkUserHasTheRefreshToken)(req, res, next);
});

test('isAuthorized: req.user is undefined', t => {
  const next = sinon.stub();

  const req = createRequest({
    user: undefined,
  });

  const res = createResponse();

  isAuthorized(req, res, next);
  t.equal(res.statusCode, 401);
  t.true(next.notCalled);
  
  t.end();
});

test('isAuthorized: req.user is there', t => {
  const next = sinon.stub();

  const req = createRequest({
    user: 'user',
  });

  const res = createResponse();

  isAuthorized(req, res, next);
  t.equal(res.statusCode, 200);
  t.true(next.calledOnce);
  
  t.end();
});