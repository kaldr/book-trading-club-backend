const bcrypt = require('bcryptjs');

const {newTokens, bearerToken} = require('./jwtAuth');
const {
  findUserByName,
  saveRefreshTokenRandomTokenHash,
  deleteRefreshToken,
  addNewUser,
} = require('../db/queries');

async function signUp(req, res, next) {
  try {
    if (req.user) {
      return res.sendStatus(400);
    }

    const username = req.body.username ? req.body.username.trim().toLowerCase() : undefined;
    const password = req.body.password;

    if (!username || !password) {
      return res.sendStatus(400);
    }

    const user = await findUserByName(username);
    if (user) {
      return res.status(401).send({message: 'username already taken'});
    }

    const passHash = await bcrypt.hash(password, 12);

    const userId = await addNewUser(username, passHash);

    await setTokensAndUser(req, res, {id: userId, name: username});

    res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
};

async function signIn(req, res, next) {
  try {
    if (req.user) {
      return res.sendStatus(400);
    }

    const username = req.body.username ? req.body.username.trim().toLowerCase() : undefined;
    const password = req.body.password;

    if (!username || !password) {
      return res.sendStatus(400);
    }

    const user = await findUserByName(username);
    if (!user) {
      return res.status(401).send({message: 'username is incorrect'});
    }

    const match = await bcrypt.compare(password, user.passHash);
    if (!match) {
      return res.status(401).send({message: 'password is incorrect'});
    }

    await setTokensAndUser(req, res, {id: user.id, name: user.name});

    res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
};

async function setTokensAndUser(req, res, user) {
  const {at, rt, randomToken} = await newTokens(user);
  const refreshTokenRandomTokenHash = await bcrypt.hash(randomToken, 12);
  await saveRefreshTokenRandomTokenHash(user.id, refreshTokenRandomTokenHash);
  req.user = user;
  res.header('Authorization', bearerToken(at, rt));
}

async function signOut(req, res, next) {
  try {
    await deleteRefreshToken(req.user.id);
    req.user = undefined;
    res.header('Authorization', '');
    res.sendStatus(200);
  } catch (err) {
    next(err);
  }
}

module.exports = {
  signUp,
  signIn,
  signOut,
};