-- DROP SCHEMA IF EXISTS book_trading_club CASCADE;
CREATE SCHEMA IF NOT EXISTS book_trading_club;

CREATE TABLE IF NOT EXISTS book_trading_club.user (
    user_id UUID PRIMARY KEY,
    user_name TEXT UNIQUE NOT NULL,
    user_pass_hash TEXT NOT NULL,
    user_refresh_token_random_token_hash TEXT,
    user_full_name TEXT,
    user_city TEXT,
    user_state TEXT,
    user_created_at TIMESTAMP WITH TIME ZONE DEFAULT now()
);

CREATE TABLE IF NOT EXISTS book_trading_club.book (
    book_id TEXT PRIMARY KEY,
    book_title TEXT NOT NULL,
    book_thumbnail_url TEXT
);

CREATE TABLE IF NOT EXISTS book_trading_club.user_book (
    user_book_id UUID PRIMARY KEY,
    book_id TEXT REFERENCES book_trading_club.book (book_id),
    user_id UUID REFERENCES book_trading_club.user (user_id),
    book_added_at TIMESTAMP WITH TIME ZONE DEFAULT now()
);

CREATE TABLE IF NOT EXISTS book_trading_club.trade (
    trade_id UUID PRIMARY KEY,
    trade_initiater_id UUID REFERENCES book_trading_club.user (user_id),
    user_book_id UUID REFERENCES book_trading_club.user_book (user_book_id),
    trade_accepted BOOLEAN NOT NULL DEFAULT FALSE,
    trade_added_at TIMESTAMP WITH TIME ZONE DEFAULT now()
);
