const {Pool} = require('pg');

const pool = new Pool({
  host: '127.0.0.1',
  user: 'postgres',
  database: 'postgres',
  password: process.env.POSTGRES_PASSWORD || 'password',
  connectionTimeoutMillis: 5_000,
  idleTimeoutMillis: process.env.NODE_ENV === 'production' ? 10_000 : 500,
});

exports.query = (text, params) => pool.query(text, params);
exports.pool = pool;