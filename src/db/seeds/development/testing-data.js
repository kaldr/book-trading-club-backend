const db = require('../../db');

const emptyTables = () => db.query(
  `TRUNCATE book_trading_club.user,
            book_trading_club.book,
            book_trading_club.user_book,
            book_trading_club.trade;`
);

const insertUsers = () => db.query(
  `INSERT INTO book_trading_club.user VALUES (
    'b5ec86cc-b78c-4f37-8335-07633fac8d5d',
    'test1',
    '$2a$10$IueETy63UhRxTOwIbd59TunGDmCb4HSTxZ3gGajg3QkNdp00mxW7K', -- pass 1234
    DEFAULT,
    'John Due',
    'City',
    'State'
  ), (
    '251e3325-920a-4147-a531-a68d0831f210',
    'test2',
    '$2a$10$mOuDCtqK919qWGth7E0ui.xi98Da48GCwm19nEYAhw2e182um8zJW', -- pass 12345
    DEFAULT,
    DEFAULT,
    DEFAULT,
    DEFAULT
  );`
);

const insertBooks = () => db.query(
  `INSERT INTO book_trading_club.book VALUES (
    'tcSMCwAAQBAJ',
    'Harry Potter and the Cursed Child – Parts One and Two (Special Rehearsal Edition)',
    'http://books.google.com/books/content?id=tcSMCwAAQBAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'
  ), (
    'pD6arNyKyi8C',
    'The Hobbit',
    'http://books.google.com/books/content?id=pD6arNyKyi8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'
  ), (
    'aWZzLPhY4o0C',
    'The Fellowship of the Ring',
    'http://books.google.com/books/content?id=aWZzLPhY4o0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api'
  );`
);

const insertUserBooks = () => db.query(
  `INSERT INTO book_trading_club.user_book VALUES (
    'd702c47f-a336-447e-b16a-1df47dac4383',
    'tcSMCwAAQBAJ', -- Harry Potter and the Cursed Child
    'b5ec86cc-b78c-4f37-8335-07633fac8d5d', -- test1
    '2017-05-13T09:17:18.470Z'
  ),
  (
    '9b7a98e3-7089-48c5-aae7-a0341cd73dbd',
    'pD6arNyKyi8C', -- The Hobbit
    'b5ec86cc-b78c-4f37-8335-07633fac8d5d', -- test1
    '2017-10-12T09:17:18.470Z'
  ),
  (
    '9029e7f6-782c-405c-b5f8-cfe58fac9236',
    'aWZzLPhY4o0C', -- The Fellowship of the Ring
    '251e3325-920a-4147-a531-a68d0831f210', -- test2
    '2017-05-11T09:17:18.470Z'
  );`
);

const insertTrades = () => db.query(
  `INSERT INTO book_trading_club.trade VALUES (
    'b31e5f17-358c-45a2-b9ae-03e4c542f692',
    'b5ec86cc-b78c-4f37-8335-07633fac8d5d', -- test1
    '9029e7f6-782c-405c-b5f8-cfe58fac9236', -- test2 The Fellowship of the Ring
    FALSE
  ),
  (
    '6fb30f3d-18b4-4101-a43a-1324a3dd45cc',
    '251e3325-920a-4147-a531-a68d0831f210', -- test2
    '9b7a98e3-7089-48c5-aae7-a0341cd73dbd', -- test1 The Hobbit
    TRUE
  );`
);

async function seed() {
  await emptyTables();
  await Promise.all([insertUsers(), insertBooks()]);
  await insertUserBooks();
  await insertTrades();
}

exports.seed = seed;