const v4 = require('uuid/v4');
const bcrypt = require('bcryptjs');

const db = require('./db');

function updateProfile(userId, userInfo) {
  const query = `UPDATE book_trading_club.user SET
    (user_full_name, user_city, user_state) = ($1, $2, $3) WHERE user_id = $4;`;

  return db.query(query, [userInfo.fullname, userInfo.city, userInfo.state, userId]);
}

async function addBookToDB(userId, book) {
  const {id, title, thumbnailUrl} = book;
  const userBookId = v4();

  const client = await db.pool.connect();
  try {
    await client.query('BEGIN');
    await client.query(
      `INSERT INTO book_trading_club.book VALUES ($1, $2, $3) ON CONFLICT DO NOTHING;`,
      [id, title, thumbnailUrl ? thumbnailUrl : null]
    );
    await client.query(
      `INSERT INTO book_trading_club.user_book (user_book_id, user_id, book_id) VALUES ($1, $2, $3);`,
      [userBookId, userId, id]
    );
    await client.query('COMMIT');
  } catch (err) {
    await client.query('ROLLBACK');
    throw err;
  } finally {
    client.release();
  }

  return userBookId;
}

async function isTradeValid(tradeId, userId) {
  const {rows: [{count}]} = await db.query(
    `SELECT count(*) FROM book_trading_club.trade AS t
     INNER JOIN book_trading_club.user_book AS ub
      ON t.user_book_id = ub.user_book_id
     WHERE t.trade_id = $1 AND ub.user_id = $2 AND t.trade_accepted = FALSE;`,
    [tradeId, userId]
  );

  return count === '1';
}

function acceptTrade(tradeId) {
  return db.query(
    `UPDATE book_trading_club.trade SET trade_accepted = TRUE WHERE trade_id = $1;`,
    [tradeId]
  );
}

function cancelTrade(tradeId) {
  return db.query(
    `DELETE FROM book_trading_club.trade WHERE trade_id = $1;`,
    [tradeId]
  );
}

async function getAllBooks() {
  const result = await db.query(
    `SELECT u.user_name AS "owner", ub.user_book_id AS "userBookId",
      t.trade_accepted AS "tradeAccepted", b.book_id AS "id",
      b.book_title AS "title", b.book_thumbnail_url AS "thumbnailUrl"
     FROM book_trading_club.user_book AS ub
     INNER JOIN book_trading_club.user AS u ON ub.user_id = u.user_id
     INNER JOIN book_trading_club.book AS b ON ub.book_id = b.book_id
     LEFT JOIN book_trading_club.trade AS t ON ub.user_book_id = t.user_book_id
     ORDER BY ub.book_added_at DESC;`);
  return result.rows;
}

async function addTrade(userId, userBookId) {
  const tradeId = v4();
  await db.query(
    `INSERT INTO book_trading_club.trade (trade_id, trade_initiater_id, user_book_id)
      VALUES ($1, $2, $3);`,
    [tradeId, userId, userBookId]
  );
  return tradeId;
}

async function bookOwnerAndIsBookInTradingProcess(userBookId) {
  const ownerPromise = db.query(
    `SELECT user_id AS "owner" FROM book_trading_club.user_book
      WHERE user_book_id = $1;`,
    [userBookId]
  );

  const bookIsInTradingProcessPromise = db.query(
    `SELECT count(*) FROM book_trading_club.trade WHERE user_book_id = $1;`,
    [userBookId]
  );

  const [ownerResult, {rows: [{count}]}] = await Promise.all([
    ownerPromise,
    bookIsInTradingProcessPromise,
  ]);

  return {
    owner: ownerResult.rows.length ? ownerResult.rows[0].owner : undefined,
    bookIsInTradingProcess: count === '1',
  };
}

async function profileInfo(userId) {
  const userInfoPromise = db.query(
    `SELECT user_full_name AS "fullname", user_city AS "city", user_state AS "state"
     FROM book_trading_club.user WHERE user_id = $1;`,
    [userId]
  );

  const userTradesPromise = db.query(
    `SELECT t.trade_accepted AS "accepted", b.book_id AS "id",
      b.book_title AS "title", b.book_thumbnail_url AS "thumbnailUrl", u.user_name AS "owner"
     FROM book_trading_club.trade AS t
     INNER JOIN book_trading_club.user_book AS ub ON ub.user_book_id = t.user_book_id
     INNER JOIN book_trading_club.book AS b ON ub.book_id = b.book_id
     INNER JOIN book_trading_club.user AS u ON ub.user_id = u.user_id
     WHERE t.trade_initiater_id = $1 ORDER BY t.trade_added_at DESC;`,
    [userId]
  );

  const tradesForUserPromise = db.query(
    `SELECT t.trade_accepted AS "accepted", b.book_id AS "id",
      b.book_title AS "title", b.book_thumbnail_url AS "thumbnailUrl",
      u.user_name AS "tradeInitiater", t.trade_id AS "tradeId"
     FROM book_trading_club.user_book AS ub
     INNER JOIN book_trading_club.trade AS t ON ub.user_book_id = t.user_book_id
     INNER JOIN book_trading_club.book AS b ON ub.book_id = b.book_id
     INNER JOIN book_trading_club.user AS u ON t.trade_initiater_id = u.user_id
     WHERE ub.user_id = $1 ORDER BY t.trade_added_at DESC;`,
    [userId]
  );

  const [userInfoResult, userTradesResult, tradesForUserResult] = await Promise.all([
    userInfoPromise,
    userTradesPromise,
    tradesForUserPromise,
  ]);

  return {
    userInfo: userInfoResult.rows[0],
    userTrades: userTradesResult.rows,
    tradesForUser: tradesForUserResult.rows,
  };
}

async function findUserByName(username) {
  const {rows: [user]} = await db.query(
    `SELECT user_id AS "id", user_name AS "name", user_pass_hash AS "passHash"
     FROM book_trading_club.user WHERE user_name = $1`,
    [username]
  );
  return user;
}

function saveRefreshTokenRandomTokenHash(userId, tokenHash) {
  return db.query(
    `UPDATE book_trading_club.user SET user_refresh_token_random_token_hash = $1 WHERE user_id = $2;`,
    [tokenHash, userId]
  );
}

function deleteRefreshToken(userId) {
  return db.query(
    `UPDATE book_trading_club.user SET user_refresh_token_random_token_hash = NULL WHERE user_id = $1;`,
    [userId]
  );
}

async function checkUserHasTheRefreshToken(userId, token) {
  const {rows: [{tokenHash}]} = await db.query(
    `SELECT user_refresh_token_random_token_hash AS "tokenHash" FROM book_trading_club.user WHERE user_id = $1`,
    [userId]
  );
  return bcrypt.compare(token, tokenHash);
}

async function addNewUser(username, passHash) {
  const userId = v4();
  await db.query(
    `INSERT INTO book_trading_club.user (user_id, user_name, user_pass_hash)
      VALUES ($1, $2, $3);`,
    [userId, username, passHash]
  );
  return userId;
}

module.exports = {
  updateProfile,
  addBookToDB,
  isTradeValid,
  acceptTrade,
  cancelTrade,
  getAllBooks,
  addTrade,
  bookOwnerAndIsBookInTradingProcess,
  profileInfo,
  findUserByName,
  saveRefreshTokenRandomTokenHash,
  deleteRefreshToken,
  checkUserHasTheRefreshToken,
  addNewUser,
};