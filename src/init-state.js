const initState = getAllBooks => async (req, res, next) => {
  try {
    const response = {
      name: '',
      books: [],
    };

    if (req.user) {
      response.name = req.user.name;
    }

    response.books = await getAllBooks();

    res.send(response);
  } catch (err) {
    return next(err);
  }
};

module.exports = {
  initState,
};