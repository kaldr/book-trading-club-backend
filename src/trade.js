const trade = (bookOwnerAndIsBookInTradingProcess, addTrade) => async (req, res, next) => {
  try {
    const {userBookId} = req.body;
    if (!userBookId) {
      return res.sendStatus(400);
    }

    const {owner, bookIsInTradingProcess} = 
      await bookOwnerAndIsBookInTradingProcess(userBookId);
    
    if (owner === undefined || owner === req.user.id || bookIsInTradingProcess) {
      return res.sendStatus(400);
    }

    await addTrade(req.user.id, userBookId);
    return res.sendStatus(201);
  } catch (err) {
    return next(err);
  }
};

const cancel = (isTradeValid, cancelTrade) => async (req, res, next) => {
  try {
    const {tradeId} = req.params;
    if (!tradeId) {
      return res.sendStatus(400);
    }

    const isValid = await isTradeValid(tradeId, req.user.id);
    if (!isValid) {
      return res.sendStatus(400);
    }

    await cancelTrade(tradeId);
    return res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
};

const accept = (isTradeValid, acceptTrade) => async (req, res, next) => {
  try {
    const {tradeId} = req.params;
    if (!tradeId) {
      return res.sendStatus(400);
    }

    const isValid = await isTradeValid(tradeId, req.user.id);
    if (!isValid) {
      return res.sendStatus(400);
    }

    await acceptTrade(tradeId);
    return res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
};

module.exports = {
  trade,
  cancel,
  accept,
};