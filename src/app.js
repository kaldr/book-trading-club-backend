const express = require('express');

const routes = require('./routes');

const urlPrefix = '/api';

const app = express();

app.use(express.json());

app.use(urlPrefix, routes);

app.use((req, res, next) => {
  res.sendStatus(404);
});

app.use((err, req, res, next) => {
  res.sendStatus(500);
});

module.exports = {
  app,
  urlPrefix,
};